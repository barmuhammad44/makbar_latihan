const express = require('express')
const app = express()
require('dotenv').config()
// const port = 3000
const port = process.env.PORT || 3000
const bodyparser=require('body-parser')
const carsRoute = require('./route/car_route')


app.use(express.json())
app.use(express.urlencoded({
    extended: false
}))

app.use('/cars', carsRoute)

app.get('/', (req,res)=> res.send('helo dunia'))

app.listen(port, ()=> console.log(`listening port ${port}`))