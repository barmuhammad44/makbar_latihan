'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    return queryInterface.bulkInsert('Cars', [{
      name: 'Avanza 1.3 G',
      kubikasi_mesin: 1300,
      transmisi: 'manual',
      penggerak_roda: 'rwd',
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    {
      name: 'Inova',
      kubikasi_mesin: 1300,
      transmisi: 'automatic',
      penggerak_roda: 'fwd',
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    ]);
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('Cars', null, {});
  }
};
